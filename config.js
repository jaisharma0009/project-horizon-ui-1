'use strict';

module.exports = {
  // relative to the root dir
  paths: {
    app: 'app',
    dist: 'dist',
    siteDist: '../site/dist',
    site: '../site',
    siteZip: '../site.zip',
    static: 'static',

    bower_components: 'bower_components'
  }
};
