var template = require('./template.html');
var MarketingInformationController = require('./controller');

let MarketingInformationComponent = {
  restrict: 'E',
  template,
  controller: MarketingInformationController,
  controllerAs: 'marketing',
  bindings: {
    account: '=',
    class: '@'
  }
};

module.exports = MarketingInformationComponent;