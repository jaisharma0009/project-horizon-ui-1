'use strict';
import _ from 'lodash';

/**
 * Directive which sets a class based on an event or value change.
 */
export class ClassOnDirective {
  attribute = 'classOn';
  restrict = 'A';

  constructor() {
    'ngInject';
  }

  condition(scope, element, attrs) {
    return false;
  }

  watcher(scope, element, attrs, callback) {
    attrs.$observe(this.attribute, (value) => {
      callback();
    });
  }

  compile(element, attrs) {
    // Directive attribute is removed to avoid indefinite loop
    element.removeAttr(_.kebabCase(this.attribute));
  }

  link(scope, element, attrs) {
    const apply = () => {
      let classes = this.getClases(scope, element, attrs);
      if (this.condition(scope, element, attrs)) {
        element.addClass(classes);
      } else {
        element.removeClass(classes);
      }
    };

    this.watcher(scope, element, attrs, apply);
  }

  getClases(scope, element, attrs) {
    return attrs[this.attribute];
  }
}

/**
 * Sets a specified class
 */
export class ClassOnStateDirective extends ClassOnDirective {
  attribute = 'classOnState';

  constructor($rootScope, $state, $transitions) {
    'ngInject';

    super();

    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$transitions = $transitions;
  }

  link(scope, element, attrs) {
    this.$transitions.onSuccess({}, (transition) => {
      let fromState = transition.$from()
        , toState = transition.$to();

      if (fromState.data && fromState.data.cssClasses) {
        element.removeClass(fromState.data.cssClasses);
      }

      if (toState.data && toState.data.cssClasses) {
        element.addClass(toState.data.cssClasses);
      }
    });
  }
}
