'use strict';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default,
  angularPromiseTracker = require('angular-promise-tracker'),
  dndLists = require('angular-drag-and-drop-lists');

module.exports = angular.module('app.kanban', [
  angularUiRouter,
  angularPromiseTracker,
  'dndLists',
  require('tspercentage').name,
  require('kanbanbadge').name,
  require('jqfootable').name,
  require('jqsparkline').name,
  require('tsbusy').name,
  require('tsstatus').name,
  require('tsplatform').name,
  require('tsonerrorsrc').name,
  require('tsauth').name
])
  .config(function ($stateProvider) {
    $stateProvider
      .state('kanban', {
        url: '/kanban',
        component: 'kanban',
        data: { cssClasses: 'narrow-body' }
      });
  })
  .component('kanban', require('./kanban.component'))
  .filter('quarterFilter', require('./quarterFilter'));
