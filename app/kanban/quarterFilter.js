
module.exports = function($sce){
  return function (input) {
    let array = input.split(' ');
    return $sce.trustAsHtml(`${array[0]} <span>${array[1]}</span>`);
  }
}