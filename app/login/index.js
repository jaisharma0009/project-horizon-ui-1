'use strict';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default;

module.exports = angular.module('app.login', [
  angularUiRouter,
  require('tsauth').name
])
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        template: require('./template.html'),
        controller: 'LoginCtrl',
        params: {
          error_reason: null
        },
        resolve: {
          PreviousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                Name: $state.current.name,
                Params: $state.params,
                URL: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      });
  })
  .controller('LoginCtrl', require('./controller'));
