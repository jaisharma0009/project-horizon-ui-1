'use strict';

var angular = require('angular');

/**
 * Directive for authenticating with Apigee SSO
 */
module.exports = angular.module('app.components.tssearch', [])
  .directive('tsSearch', function() {
    return {
      restrict: 'E',
      scope: {},
      template: require('./template.html'),
      controller: 'tsSearchCtrl'
    };
  })
  .controller('tsSearchCtrl', require('./controller'));