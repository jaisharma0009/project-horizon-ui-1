'use strict';

var angular = require('angular');
var uiGrid = require('angular-ui-grid').default;
/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.tsuigrid', [
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.pagination'
])
  .directive('tsUiGrid', function() {
    return {
      restrict: 'E',
      scope: {
        options: '&',
        title: '@'
      },
      bindToController: true,
      // link: function(scope, element, attrs) {

      //   scope.options = attrs.
            
      // },
      // transclude: true,
      template: require('./template.html'),
      controller: 'tsUiGridCtrl'
    };
  })
  .controller('tsUiGridCtrl', require('./controller'));
