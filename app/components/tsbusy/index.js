'use strict';

var angular = require('angular');

/**
 * Directive for displaying a busy indicator
 */
module.exports = angular.module('app.components.tsbusy', [])
  .directive('tsBusy', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        tracker: '='
      },
      template: require('./template.html')
    };
  });
