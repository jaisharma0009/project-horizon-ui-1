'use strict';

var angular = require('angular');
var $ = require('jquery');
var _ = require('lodash');

module.exports = angular.module('app.components.tsclickoutside', [])
  .directive('tsClickOutside', function ($rootScope, $timeout) {
    return {
      restrict: 'A',
      scope: {
        onClickOut: '&',
        tsClickOutside: '&',
        ngClickOutside: '&'
      },
      link: function (scope, element, attrs, ctrl) {

        element.on('click', function(event){
          event.stopPropagation();
        })

        $(document).bind('click', function() {
          scope.onClickOut();
          scope.tsClickOutside();
          scope.ngClickOutside();
        });
      }
    }
  });
