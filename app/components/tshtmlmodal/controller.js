'use strict';

module.exports = class TsHtmlModalController {
  constructor ($scope, $uibModal, $http, $window) {
  'ngInject';

  $scope.open = function () {

    $scope.promise().then(function (response) {
      // debugger;
      var data = response;
      
      if($scope.jsonPath) {
        var jsonArr = $scope.jsonPath.split('.');

        angular.forEach(jsonArr, function(item) {
          data = data[item]
        });
      }

      var options = {
        src: data,
        height: $window.innerHeight - 70
      };

      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: `<iframe style="width: 100%; height:{{height}}px" id="modalIframe" srcdoc="{{src}}"></iframe></div>`,
        size: $scope.size || 'lg',
        resolve: {
          options: function () {
            return options;
          }
        },
        controller: function ($scope, $uibModalInstance, options, $sce) {
          $scope.src = $sce.trustAsHtml(options.src);
          $scope.height = options.height

          $scope.close = function () {
            $uibModalInstance.dismiss();
          }
        }
      });

      modalInstance.result.then($scope.onDismiss, $scope.onClose);
    });
  }
}
};
