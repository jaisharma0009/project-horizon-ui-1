'use strict';

var angular = require('angular');

/**
 * Directive for custom selects
 */
module.exports = angular.module('app.components.tsselect', [])
  .directive('tsSelect', function() {
    return {
      restrict: 'E',
      scope: {
        offset: '@',
        display: '@',
        selectValue: '@',
        data: '=',
        model: '=',
        onSelect: '&'
      },
      transclude: true,
      template: require('./template.html'),
      controller: 'tsSelectCtrl'
    };
  })
  .controller('tsSelectCtrl', require('./controller'));
