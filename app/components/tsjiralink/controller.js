'use strict';

/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.$watch('jira_issue_no', function(jira_issue_no) {
    $scope.jira_issue_no = null;

    if (typeof jira_issue_no === 'string' && jira_issue_no !== 'Unspecified') {  
        $scope.jira_issue_no = jira_issue_no;
      
    }
  });
};
