'use strict';

var angular = require('angular'),
  $ = require('jquery'),
  jquerySparkline = require('jquery.sparkline');

/**
 * Directive for displaying a sparkline
 */
module.exports = angular.module('app.components.jqsparkline', [])
  .directive('jqSparkline', function() {
    return {
      restrict: 'E',
      scope: {
        data: '=',
        options: '@'
      },
      link: function(scope, elem, attrs) {
        scope.$watch('data', function(newValue) {
          var options = scope.$root.jqSparklineOptions[scope.options];
          options.fillColor = '#F9F9F9';
          options.lineColor = '#999';
          options.lineWidth = 1;

          if (!newValue) {
            return;
          }

          if (typeof newValue === 'string') {
            newValue = newValue.split(',').map(function(val) {
              return parseInt(val);
            });
          }

          if (options && options.zeroAsNull) {
            newValue.forEach(function(val, idx, array) {
              if (val === 0) {                
                array[idx] = null;
              }
            });
          }

          $(elem).sparkline(newValue, options);
        });
      }
    };
  });
