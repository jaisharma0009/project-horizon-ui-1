'use strict';

var angular = require('angular');

/**
 * Directive for displaying an avatar from an email address
 */
module.exports = angular.module('app.components.tsavatar', [])
  .directive('tsAvatar', function() {
    return {
      restrict: 'E',
      scope: {
        email: '@'
      },
      template: require('./template.html'),
      controller: 'tsAvatarCtrl'
    };
  })
  .controller('tsAvatarCtrl', require('./controller'));
