'use strict';

var angular = require('angular');

/**
 * Directive for displaying a stoplight-style status indicator
 */
module.exports = angular.module('app.components.tsentitlementstatus', [])
  .directive('tsEntitlementstatus', function() {
    return {
      restrict: 'E',
      scope: {
        entitled: '=',
        used: '='
      },
      template: require('./template.html'),
      controller: 'tsEntitlementstatusCtrl'
    };
  })
  .controller('tsEntitlementstatusCtrl', require('./controller'));
