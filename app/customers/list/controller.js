'use strict';

var _ = require('lodash');
var $ = require('jquery');
var solve = require('logician');
var isNumber = require('is-number');
var deepEqual = require('deep-equal');

var paginationPageSizes = require('../../values.js').paginationSizes;

_.sum = function (arr) {
	var total = 0;
	if (!arr || !Array.isArray(arr)) return total;
	var val;
	_.each(arr, item => {
		val = parseInt(item, 10);
		if (!isNaN(val)) total += val;
	});
	return total;
}


var customerStatePopupText = function (state) {

	let states = {
		'Upset': 'Accounts we believe are upset with us. Looks at support health and P1 tickets in a 30 day period.',
		'Adopting': 'Accounts that are new and onboarding Apigee. They are a stage 6 customer within a 90 day period from sales and their traffic is under 500 calls for the previous 30 days.',
		'Stalled': 'Accounts that are stalled by not seemingly progressing enough with on-boarding Apigee (adopting), not developing (development), no org activity (operation), little traffic growth (momentum).',
		'Heavy Momentum': 'Accounts that have considerable traffic gains over the previous week and also have dev and operational activity showing.',
		'Growing': 'Accounts that have steady growth and are not met by one of the other states.',
		'Private Cloud': 'On Prem account where we do not have enough information to judge customer state currently.'
	};

	let base_state = state;
	if (state && state.indexOf(' (') > -1) {
		base_state = state.substr(0, state.indexOf(' ('));
	}

	if (states.hasOwnProperty(base_state)) {
		return states[base_state];
	} else {
		return "Customer is in a '" + state + '" state.';
	}

}


/**
 * @ngInject
 */
module.exports = class ListController {
	constructor($scope, $state, $window, $timeout, Restangular, promiseTracker, $location,
		jqSparklineOptions, uiGridGroupingConstants, contextService, $http, $rootScope, $animate,
		uiGridExporterConstants, $templateCache, presetsService, $stateParams, $uibModal, $interval,
		TECHNICAL_COMPETENCY_RATINGS, $filter, kanbanService) {
		'ngInject';

		$scope.visibleTable = true;
		$scope.jqSparklineOptions = jqSparklineOptions;
		$scope.visibleGridDropdown = { visible: false };
		this.scope = $scope;
		this.Restangular = Restangular;
		this.uiGridExporterConstants = uiGridExporterConstants;
		this.$timeout = $timeout;
		this.presetsService = presetsService;
		this.kanbanService = kanbanService;
		this.$location = $location;
		this.$rootScope = $rootScope;
		this.$modal = $uibModal;
		this.savedExternal = false;
		this.showExport = false;
		this.myNumberFilter = $filter('myNumberFilter');
		this.myTextNullFilter = $filter('myTextNullFilter');
		this.technicalCompetencyFilter = $filter('technicalCompetencyFilter');
		this.TECHNICAL_COMPETENCY_RATINGS = TECHNICAL_COMPETENCY_RATINGS;

		var self = this;

		var stateName = 'customers';
		$rootScope.$watch('$state.current.name', function (newVal) {
			$scope.visibleTable = (newVal == stateName);
		}, true);

		var filters = {
			teams: [
				'All'
			],
			signals: [],
			expansionRatingModels: [],
			groupby: [
				{
					displayName: 'CS Region',
					name: 'services_pod'
				}, {
					displayName: 'Owner Region',
					name: 'region'
				}, {
					displayName: 'None',
					name: 'None'
				}
			]
		};
		filters.selectedSignal = filters.signals[0];
		filters.selectedTeam = filters.teams[0];
		filters.selectedGroupby = filters.groupby[2];
		filters.selectedExpansionRatingModel = null;

		this.contextService = contextService;

		$scope.filters = filters;

		this.getPresets();

		contextService.getCsRegions()
			.then(regions => {
				_.each(regions, region => {
					filters.teams.push(region.services_pod);
				})
			});

		contextService.getExpansionRatingModels()
			.then(models => {
				$scope.expansionRatingModels = models;
				_.each(models, model => {
					var simpleModel = { displayName: model['name'], id: model['id'] };
					filters.expansionRatingModels.push(simpleModel);
					if(model.name.toLowerCase() == 'EXECS, VISION, SENTIMENT & $'.toLowerCase()) filters.selectedExpansionRatingModel = simpleModel;
				});
				if(!filters.selectedExpansionRatingModel) filters.selectedExpansionRatingModel = filters.expansionRatingModels[0];
			})

		$scope.$on('MODEL_SELECTED', (e, model) => {
			let newModel = _.find(this.scope.filters.signals, s => s.id == model.id);

			if (model.id == 'all') {
				newModel = model;
			}

			$scope.originalModel = newModel;
			this.presetChange(newModel);
		});

		$scope.$watch('filters.selectedExpansionRatingModel', function (newval, oldval) {

			if (self.scope.gridAccounts && !self.scope.gridAccounts.loading) self.expansionRatingModelChange(newval, oldval);

		}, true);

		$scope.$watch('filters.selectedSignal', function (newval, oldval) {
			self.presetChange(newval, oldval);
		}, true);

		$scope.$watch('filters.selectedTeam', function (newval, oldval) {

			if (newval == 'All')
				newval = null;

			if(!!$scope.accountListGrid) {
				$scope.accountListGrid.grid.getColumn('services_pod').filters[0] = {
					term: newval
				};
			}

		}, true);

		$scope.$watch('filters.selectedGroupby', function (newval, oldval) {
			if (oldval != newval && !!$scope.accountListGrid) {
				$scope.accountListGrid.grouping.clearGrouping();
				if (newval.name != 'None') {
					$scope.accountListGrid.grouping.groupColumn($scope.filters.selectedGroupby.name);
				} else {
					if ($scope.accountListGrid.gridApi) {
						$scope.accountListGrid.gridApi.expandable.collapseAllRows();
					}
				}
			}
		}, true);

		$scope.customersTracker = promiseTracker();
		$scope.accountListGrid = null;
		$scope.grouping = true;

		$scope.$on('EXPANSION_TARGET_UPDATED', (evnt, data) => {
			this.updateExpansionTarget(data.id, data.expansion_target);
		});

		$scope.gridAccounts = {
			gridName: 'listGrid',
			paginationPageSize: paginationPageSizes.default,
			paginationPageSizes: paginationPageSizes.sizes,
			enablePaginationControls: true,
			// useExternalPagination: true,
			// useExternalSorting: true,
			loading: true,
			data: [],
			enableFiltering: true,
			exporterCsvFilename: 'myFile.csv',
			enableGridMenu: true,
			exporterMenuPdf: false,
			enableColumnMenus: true,
			enableColumnResizing: true,
			rowHeight: 30,
			columnDefs: [
				{
					displayName: 'Region',
					name: 'region',
					field: 'region',
					cellTooltip: true,
					// field: 'entity_json.Owner_Area__c',
					width: '7%',
					visible: false
				}, {
					displayName: 'CS Region',
					field: 'services_pod',
					name: 'services_pod',
					width: '7%'
				}, {
					cardIndex: 1,
					displayName: 'Name',
					field: 'account_name',
					cellTooltip: true,
					// cellTemplate: '<a class="ui-grid-cell-contents" target="_360" href="https://360.apigee.com/internal/#/customers/{{row.entity.id}}">{{row.entity.account_name}}</a><a ng-if="!row.groupHeader" class="ui-grid-cell-contents" href="/#!/customers/{{row.entity.id}}">...</a>'
					cellTemplate: '<a ng-if="!row.groupHeader" class="ui-grid-cell-contents" ui-sref="customers.detail.overview({accountId: row.entity.account_id})">{{row.entity.account_name}}</a>',
					width: '15%'
				}, {
					displayName: 'Target',
					field: 'expansion_target',
					name: 'expansion_target',
					type: 'number: 2',
					filter: { 'condition': this.myNumberFilter },
					headerTooltip: '12 Month Expansion Target',
					width: '7%'
				}, {
					displayName: 'Priority',
					field: 'priority_score',
					cellTooltip: true,
					headerTooltip: 'Expansion Scoring Priority',
					type: 'number',
					filter: { 'condition': this.myNumberFilter },
					cellTemplate: '<a class="ui-grid-cell-contents" ui-sref="customers.detail.expansion-rating({accountId: row.entity.account_id})">{{row.entity.priority_score | number: 0}}</a>',
					width: '5%'
				}, {
					displayName: 'Customer State',
					field: 'customer_state',
					// cellTooltip: '{{customerStatePopupText(row.entity.customer_state)}}',
					filter: { 'condition': this.myTextNullFilter, },
					headerTooltip: 'Customer State',
					// customerStatePopupText,
					// cellTemplate: '<a class="ui-grid-cell-contents" ng-if="row.entity.customer_state" uib-tooltip-html="customerStatePopupText(row.entity.customer_state)" ui-sref="customers.detail.overview({accountId: row.entity.account_id})">{{row.entity.customer_state}}</a>',
					cellTemplate: '<span class="ui-grid-cell-contents" ng-if="row.entity.customer_state" tooltip-append-to-body="true" uib-tooltip="{{row.entity.customer_state_tooltip}}">{{row.entity.customer_state}}</span>',
					width: '10%'
				}, {
					displayName: 'Magellan',
					field: 'magellan_score',
					cellTooltip: true,
					headerTooltip: 'Combined Magellan Score',
					type: 'number',
					filter: { 'condition': this.myNumberFilter },
					cellTemplate: '<a class="ui-grid-cell-contents" ng-if="row.entity.magellan_score > 0" ui-sref="customers.detail.expansion-rating({accountId: row.entity.account_id})">{{row.entity.magellan_score | number: 0}}</a>',
					width: '5%'
				}, {
					displayName: 'Execution',
					field: 'magellan_execution_score',
					cellTooltip: true,
					headerTooltip: 'Normalized Magellan Execution Score',
					type: 'number',
					filter: { 'condition': this.myNumberFilter },
					cellFilter: 'number: 0',
					visible: false,
					width: '5%'
				}, {
					displayName: 'Vision',
					field: 'magellan_vision_score',
					cellTooltip: true,
					headerTooltip: 'Normalized Magellan Vision Score',
					type: 'number',
					filter: { 'condition': this.myNumberFilter },
					cellFilter: 'number: 0',
					visible: false,
					width: '5%'
				}, {
					displayName: 'Alignment',
					field: 'magellan_alignment_score',
					cellTooltip: true,
					headerTooltip: 'Normalized Magellan Alignment Score',
					type: 'number',
					filter: { 'condition': this.myNumberFilter },
					cellFilter: 'number: 0',
					visible: false,
					width: '5%'
				}, {
					displayName: 'Tech',
					headerTooltip: 'Technical Competency',
					field: 'normalized_score',
					cellTooltip: true,
					filter: { 'condition': this.technicalCompetencyFilter },
					cellTemplate: '<a class="ui-grid-cell-contents" ng-if="!row.groupHeader" ui-sref="customers.detail.technical-competency({accountId: row.entity.account_id})">{{row.entity.normalized_rating}}</a>',
					minWidth: 100
				}, {
					displayName: 'Rep',
					field: 'sales_rep_email',
					cellTooltip: true,
					cellTemplate: '<a class="ui-grid-cell-contents" ng-if="!row.groupHeader" href="mailto:{{row.entity.sales_rep_email}}">{{row.entity.sales_rep_name}}</a>',
					minWidth: 100
				}, {
					displayName: 'Industry',
					field: 'industry',
					cellTooltip: true,
					width: '10%',
					visible: false
				}, {
					displayName: 'Traffic',
					field: 'pro_rated_percent_of_entitlement',
					headerTooltip: 'Prorated Traffic Entitlement Usage',
					filter: { 'condition': this.myNumberFilter },
					type: 'number',
					cellTemplate: '<span class="ui-grid-cell-contents" ng-if="row.entity.pro_rated_percent_of_entitlement > 0">{{(row.entity.pro_rated_percent_of_entitlement * 100) | number: 0.00}}%</span>',
					visible: true,
					width: '5%'
				}, {
					cardIndex: 2,
					displayName: ' ',
					field: '90d_traffic',
					type: 'number',
					cellTemplate: '<jq-sparkline class="grid-graph" ng-if="!row.groupHeader" data="row.entity.traffic_sparkline" options="small">',
					width: '21%',
					filter: { 'condition': this.myNumberFilter }
				}, {
					displayName: 'Support',
					field: 'support_score',
					type: 'number',
					width: '5%',
					cellClass: 'grid-center',
					cellTemplate: '<ts-status ng-if="!row.groupHeader" status="row.entity.support_score_indicator"></ts-status>',
					visible: false
				}, {
					displayName: 'Platform',
					cellTooltip: true,
					field: 'platform',
					width: '12%'
				}, {
					displayName: 'Next Opportunity',
					field: 'next_close_date',
					visible: false,
					type: 'date',
					cellFilter: 'date:"yyyy-MM-dd"'
				},

			],
			onRegisterApi: function (accountListGrid) {
				$scope.accountListGrid = accountListGrid;
				$scope.defaultState = {
					entity: $scope.accountListGrid.saveState.save(),
					title: 'All'
				}

				$interval(function () {
					$scope.accountListGrid.core.handleWindowResize();
				}, 500, 10);

				$animate.enabled(accountListGrid.grid.element, false);

				$scope.accountListGrid.grid.api.core.on.rowsRendered($scope, () => {

					if ($scope.gridAccounts && !$scope.gridAccounts.loading) {

						let newObject = $scope.accountListGrid.saveState.save();
						let base = $scope.originalModel;

						if (!$scope.selectionsLoaded) {
							$scope.selectionsLoaded = true;
							return;
						}

						if (typeof newObject == 'string') newObject = JSON.parse(newObject);
						if (typeof base == 'string') base = JSON.parse(base);

						if (!base || !base.hasOwnProperty('entity')) {
							base = $scope.defaultState;
						}

						base = base.entity;

						// if (typeof newObject != 'string') newObject = JSON.stringify(newObject);
						// if (typeof base != 'string') base = JSON.stringify(base);

						if (newObject && base) {
							$rootScope.$broadcast('GRID_CHANGED', {
								changed: !deepEqual(newObject, base),
								entity: base
							});
						}
					};
				});
			}
		};


		var limit = 1000;
		// if ($location.$$host == 'localhost') limit = 50;

		$scope.customerPlatforms = null;

		this.reloadCustomerGrid();

		$scope.$on('SAVE_MODEL', (e, name) => {
			self.savePreset(name, self);
		});

	}

	getPresets(selected = 0) {
		this.scope.newPreset = null;
		var filters = [
			{ title: 'All', name: 'all' }
		];

		this.contextService.getExplansionPlanningModels()
			.then(models => {
				if (ENV == 'local') {
					models = _.filter(models, m => m.user_id == 1);
				}

				_.each(models, function (model) {
					filters.push({ id: model.id, user_id: model.user_id, title: model.shortdesc, entity: model.entity_json })
				});

				this.scope.filters.signals = filters;

				if (!!selected) {
					let index = _.findIndex(filters, f => f.id == selected);
					selected = index != -1 ? index : 0;
				}

				this.scope.filters.selectedSignal = this.scope.filters.signals[selected];
				this.scope.filters.signals.push({ title: 'Share current', name: 'export' });

				var model = this.$location.search().model;
				var exists = _.findIndex(filters, f => f.id == model);

				if (model && exists === -1) {
					this.contextService
						.getExpansionPlanningModel(model)
						.then(m => {
							var signals = this.scope.filters.signals;
							var signal = {
								id: m.id,
								user_id: m.user_id,
								title: m.shortdesc,
								entity: m.entity_json
							};

							this.$rootScope.$broadcast('NEW_MODEL', m);

							signals.splice(signals.length - 1, 0, signal);
							this.scope.filters.selectedSignal = signal;

							this.scope.newPreset = { name: m.shortdesc };

							if (!this.saveExternal) {
								let modalInstance = this.$modal.open({
									animation: true,
									ariaLabelledBy: 'modal-title',
									ariaDescribedBy: 'modal-body',
									template: `<div>
														<div class="modal-header">
															<h4>
																Do you want to add <span class="orange-text">{{model.name}}</span>
																to your models?
															</h4>
														</div>
														<br />
														<div class="modal-body">
															<md-input-container class="col-md-12">
																<label>Model short description</label>
																<input type="text" placeholder="Model name" ng-model="name">
															</md-input-container>
															<div class="clearfix"></div>
														</div>
														<div class="modal-footer">
                							<button class="btn apigee-btn" ng-click="save()">Add</button>
															<button class="btn btn-default" ng-click="cancel()">Cancel</button>
														</div>
													 </div>`,
									controller: function ($scope, model, $uibModalInstance) {
										$scope.model = model;
										$scope.name = model.name;
										$scope.cancel = () => { $uibModalInstance.dismiss(); }
										$scope.save = () => { $uibModalInstance.close($scope.name); }
									},
									resolve: {
										model: function () {
											return {
												name: m.shortdesc
											};
										}
									}
								});

								modalInstance.result.then((name) => {
									if (name) {
										this.scope.newPreset.name = name;
									}

									this.saveExternal = true;
									this.saveShared(name);
								});
							}

						});
				} else if (model) {
					this.scope.filters.selectedSignal = this.scope.filters.signals[exists];
				}
			});

	}

	scoreExpansionRating(expansionRating, expansionRatingModel) {
		debugger;
		let score = 0;
		_.mapKeys(expansionRatingModel, function (key, item) {
			if (expansionRating.hasOwnProperty(key)) {
				let elementRating = parseInt(expansionRating[key], 10);
				let elementModel = parseInt(item, 10);

				if (!isNaN(elementModel) && !isNaN(elementRating)) score += elementModel * elementRating;

			}
		});
		return score;

	}

	expansionRatingModelChange(newval, oldval) {

		let state = this.scope.accountListGrid.saveState.save();

		let models = this.scope.expansionRatingModels;
		models = _.keyBy(models, 'id');
		let model = models[newval.id];
		let self = this;

		let scoresPromise = self.contextService.getExpansionRatings()
			.then(function (ratings) {
				// debugger;
				ratings = _.groupBy(ratings, 'account_id');
				_.each(self.scope.gridAccounts.data, function (row) {
					let expansionRating = ratings[row['account_id']];
					let updated_score = 0;
					if(expansionRating){
						expansionRating = expansionRating[0];
						if(expansionRating)
							updated_score = self.contextService.scoreExpansionRating(expansionRating, model);
					}
					row.priority_score = updated_score;
				});
				self.scope.accountListGrid.saveState.restore(self.scope, state);
			});

		self.scope.customersTracker.addPromise(scoresPromise);

	}

	presetChange(newval, oldval) {
		this.$timeout(() => {
			if (newval) {
				switch (newval.name) {
					case 'export':
						//this.exportPresets();
						this.scope.filters.selectedSignal = oldval;
						let url = this.$location.absUrl().split('?')[0];
						url = `${url}?preset=${oldval.id}`;

						this.scope.export = { url: url, name: oldval.name };
						break;

					case 'import':
						this.importPreset();
						this.scope.filters.selectedSignal = oldval;
						break;

					case 'all':
						let state = this.contextService.defaultPresetState;
						this.scope.accountListGrid.saveState.restore(this.scope, state.entity);
						break;

					default:
						let entity = newval.entity;

						if (typeof entity == "string") {
							entity = JSON.parse(entity);
						}

						let csRegion = _.find(entity.columns, { name: "services_pod" });
						let term = csRegion.filters[0].term;
						if (term) {
							let index = _.findIndex(this.scope.filters.teams, t => {
								return t.toLowerCase() == term.toLowerCase()
							});
							this.scope.filters.selectedTeam = this.scope.filters.teams[index];
						}

						this.scope.accountListGrid.saveState.restore(this.scope, entity);

						this.$timeout(() => {
							if(entity.ratingModel) {
								this.scope.filters.selectedExpansionRatingModel = entity.ratingModel;
							}
						}, 100);

						break;
				}
			}
		});
	}

	exportPreset() {
		let presets = localStorage.getItem('states');
		let encoded = encodeURIComponent(presets);
		let anchor = `<a href="data:text/json;charset=utf-8,${encoded}" download="presets.json" accept="application/json"></a>`;

		$(anchor)[0].click();
	}

	importPreset() {
		var id = prompt('Please enter the ID:', null);
		if (id) {
			var model = getExpansionPlanningModel(id);
			if (model) {
				this.scope.filters.signals.push({ id: model.id, user_id: model.user_id, title: model.shortdesc, entity: model.entity_json })
			}
		}
	}

	updateExpansionTarget(id, expansion_target) {
		let index = _.findIndex(this.scope.gridAccounts.data, (item) => item.account_id == id);

		this.scope.gridAccounts.data[index].expansion_target = expansion_target;
		this.scope.accountListGrid.grid.api.core.refresh();
	}

	exportAll() {
		this.scope.accountListGrid.exporter.csvExport(
			this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.ALL);
		this.showExport = false;
	}

	exportVisible() {
		this.scope.accountListGrid.exporter.csvExport(
			this.uiGridExporterConstants.VISIBLE, this.uiGridExporterConstants.VISIBLE);
		this.showExport = false;
	}

	toggleFilter(index) {
		if (this.scope.gridAccounts.columnDefs[index].visible === undefined) {
			this.scope.gridAccounts.columnDefs[index].visible = false;
		} else {
			this.scope.gridAccounts.columnDefs[index].visible = !this.scope.gridAccounts.columnDefs[index].visible;
		}

		this.scope.accountListGrid.grid.api.core.refresh();
		this.$rootScope.$broadcast('UI_GRID_COLUMN_CHANGE', this.scope.gridAccounts.columnDefs[index].field);
	}

	isActive(index) {
		var obj = this.scope.gridAccounts.columnDefs[index];
		if (obj.visible === undefined) return true;
		return obj.visible;
	}

	hideGridDrop() {
		this.scope.visibleGridDropdown.visible = false;
	}

	savePreset(name, self) {
		if (!self) {
			self = this;
		}

		let state = self.scope.accountListGrid.saveState.save();
		state.ratingModel = self.scope.filters.selectedExpansionRatingModel;

		self.$timeout(() => { self.$rootScope.$broadcast('MODEL_SAVED', name) }, 1000);
		self.contextService.saveExpansionPlanningModel(name, state);
		self.contextService.setExpansionPlanningSaving(name);

		self.scope.showPresetSave = false;
	}

	saveShared() {
		let name = this.scope.newPreset.name;
		this.savePreset(name);
		this.getPresets(this.scope.newPreset.id);
	}

	//Load the customer platforms first, then the customers...
	reloadCustomerGrid() {

		var orderby = ['account_name:asc'];

		var where = [
			'sales_stage:eq:6 - Customer',
			// 'platform:ne:Partner'
		];

		if (this.scope.filters.selectedTeam != 'All') {
			where.push('services_pod:eq:' + this.scope.filters.selectedTeam);
		}

		var endpoint = ENV == 'local' ? 'sfdc/cs_account_plannings' : 'cs/account_plannings';

		var customers = this.Restangular
			.all(endpoint)
			.withHttpConfig({ cache: true })
			.getList({
				where: where,
				limit: 1000,//limit,
				orderby: ['region:asc', 'account_name:asc', 'sales_rep_name:asc']
			})
			.then(list => {
				_.each(list, item => {

					if(ENV == 'local') {
						item.customer_state = 'Upset';
					}

					item['account_logo'] = '//logo.clearbit.com/' + item['website'] + '?size=60';
          if (ENV == 'local') {
            item['account_logo'] = this.kanbanService.getMockLogo();
          }

					item['has_traffic'] = item.traffic_sparkline ? (_.sum(item.traffic_sparkline.split(',')) > 0) : false;
					item['90d_traffic'] = item.has_traffic ? _.sum(item.traffic_sparkline.split(',')) : 0;
					item['normalized_rating'] = this.TECHNICAL_COMPETENCY_RATINGS[item.normalized_score];
					item['customer_state_tooltip'] = customerStatePopupText(item.customer_state);
					if (isNaN(parseFloat(item['pro_rated_percent_of_entitlement']))) {
						item['pro_rated_percent_of_entitlement'] = 0;
					}
					item.scope = this.scope;
				});

				list = _.sortBy(list, 'entity_json.Services_POD__c');

				// $scope.customers = list;
				this.scope.gridAccounts.data = list;

				this.scope.accountListGrid.grouping.clearGrouping();
				if (this.scope.filters.selectedGroupby.name != 'None') {
					this.scope.accountListGrid.grouping.groupColumn($scope.filters.selectedGroupby.name);
				} else {
					if (this.scope.accountListGrid.gridApi) {
						this.scope.accountListGrid.gridApi.expandable.collapseAllRows();
					}
				}

				this.scope.gridAccounts.loading = false;

			})
			.catch(err => { console.error(err); });
		this.scope.customersTracker.addPromise(customers);
	};
}
