// 'use strict';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default,
  angularPromiseTracker = require('angular-promise-tracker'),
  ListComponent = require('./list.component'),
  PresetsComponent = require('../models/models.component'),
  uiBootstrap = require('angular-bootstrap'),
  filters = require('../../filters');

module.exports = angular.module('app.customers.list', [
  angularUiRouter,
  angularPromiseTracker,
  'ui.grid',
  'ui.grid.pagination',
  'ui.grid.grouping',
  'ui.grid.saveState',
  'ui.grid.resizeColumns',
  uiBootstrap,
  require('jqfootable').name,
  require('jqsparkline').name,
  require('tsbusy').name,
  require('uigridheight').name,
  require('tsstatus').name,
  require('tsplatform').name,
  require('tsauth').name
])
  .config(function ($stateProvider) {
    $stateProvider
      .state('customers', {
        url: '/customers',
        component: 'list',
        data: { cssClasses: 'narrow-body' }
      });
  })
  .constant('TECHNICAL_COMPETENCY_RATINGS', ['No Recent Analysis', 'Lagging', 'Progressing', 'Competitive', 'Leading'])
  .component('list', ListComponent)
  .component('models', PresetsComponent)
  .filter('myNumberFilter', filters.myNumberFilter )
  .filter('myTextNullFilter', filters.myTextNullFilter)
  .filter('technicalCompetencyFilter', filters.technicalCompetencyFilter);
