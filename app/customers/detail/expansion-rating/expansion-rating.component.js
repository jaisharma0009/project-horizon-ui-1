var template = require('./template.html');
var ExpansionRatingController = require('./controller');

let ExpansionRatingComponent = {
  restrict: 'E',
  template,
  controller: ExpansionRatingController,
  controllerAs: 'es'
};

module.exports = ExpansionRatingComponent;