'use strict';

var moment = require('moment');
var $ = require('jquery');
var d3 = require('d3');

/**
 * @ngInject
 */
module.exports = class MagellanController {
    constructor($rootScope, $state, $scope, $location, tsAuthService, promiseTracker, 
    contextService, $window, $timeout, $stateParams) {
    'ngInject';

    var accountId = $stateParams.accountId;
    $scope.accountId = accountId;

    $scope.magellanMapsTracker = promiseTracker();
    var magellanInsances = contextService.getMagellanInstances(accountId);
    $scope.magellanMapsTracker.addPromise(magellanInsances);

    $scope.magellanSelector = {
        magellanInstances : [],
        emptyMagellan: true,
        selectedMagellanInstanceId: null
    };

    var margin = { top: 50, right: 100, bottom: 100, left: 100 },
        width = Math.min(700, $window.innerWidth - 10) - margin.left - margin.right,
        height = Math.min(width, $window.innerHeight - margin.top - margin.bottom - 20);

    var color = d3.scale.ordinal();

    $scope.radarData = {
        data: [],
        radarChartOptions: {
            w: width,
            h: height,
            margin: margin,
            maxValue: 4,
            levels: 4,
            roundStrokes: true,
            color: color
        }
    };

    magellanInsances.then(maps => {
        $scope.magellanSelector.magellanInstances = maps;
        if(Array.isArray(maps) && maps.length > 0){
            $scope.magellanSelector.emptyMagellan = false;
            $scope.magellanSelector.selectedMagellanInstance = maps[0];
        }else{
            $scope.magellanSelector.emptyMagellan = true;
        }
    });

    $scope.magellanMapTracker = promiseTracker();

    $scope.$watch('magellanSelector.selectedMagellanInstance', function(newVal, oldVal){
        if(!newVal || !newVal.id) return;
        var map_id = newVal.id;
        var magellanScores = contextService.getMagellanDimensionScores(map_id);
        $scope.magellanMapTracker.addPromise(magellanScores);
        magellanScores.then(scores => {
            $scope.radarData.data = [_.map(scores, score => {
                return {
                    "axis": score.name ? score.name : 'Dimension',
                    "value": parseInt(score.value, 10),
                    "direction": parseInt(score.direction, 10),
                    "internal_notes": score.internal_notes,
                    "public_notes": score.public_notes,
                    "id": score.id
                }
            })];
        });
    }, true);

    $scope.jqSparklineOptions = {
        type: 'line',

        width: '100px',
        height: '1.5em',
        chartRangeMin: 0,
        defaultPixelsPerValue: 2,

        lineColor: '#298fc2',
        fillColor: 'rgba(41, 143, 194, 0.1)',
        spotColor: '',
        minSpotColor: null,
        maxSpotColor: '#fc4c02',
        highlightSpotColor: '#fc4c02',
        highlightLineColor: '#fc4c02',

        disableHiddenCheck: true,
        enableTagOptions: true,

        zeroAsNull: true
    };

    // Events
    $scope.opportunitiesTracker = promiseTracker();

    $scope.gridOpportunitiesOptions = {
        data: [],
        paginationPageSize: 9,
        paginationPageSizes: [9],
        enablePaginationControls: true,
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        isCollapsed: false,
        columnDefs: [
            {
                displayName: 'Created Date',
                field: 'created_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '30%'
            }, {
                displayName: 'Opportunity',
                field: 'opportunity_name',
                width: '70%',
                cellTemplate: '<span class="ui-grid-cell-contents"><a target="_sfdc" href="https://apigee.my.salesforce.com/{{row.entity.id}}">{{row.entity.opportunity_name}}</a></span>'
            }
        ]
    };

    var opportunities = contextService.getOpportunityList(accountId)
        .then(list => {
            $scope.gridOpportunitiesOptions.data = list;
        });

    $scope.opportunitiesTracker.addPromise(opportunities);

    //Purchases
    $scope.purchasesTracker = promiseTracker();
    $scope.gridProductPurchasesOptions = {
        data: [],
        paginationPageSize: 9,
        paginationPageSizes: [9],
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        isCollapsed: false,
        columnDefs: [
            {
                displayName: 'Order Date',
                field: 'order_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                maxWidth: 100
            }, {
                displayName: 'Start Date',
                field: 'start_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                maxWidth: 100
            }, {
                displayName: 'End Date',
                field: 'end_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                maxWidth: 100
            }, {
                displayName: 'Description',
                field: 'description',
                tooltip: true
            }, {
                displayName: 'Type',
                field: 'product_type',
                maxWidth: 100
            }, {
                displayName: 'Current',
                field: 'current',
                maxWidth: 100,
                cellTemplate: '<input type="checkbox" ng-model="row.entity.current">'
            }
        ]
    };

    var purchasesProducts = contextService.getPurchaseProductList(accountId)
        .then(purchases => {
            var now = moment();
            _.each(purchases, purchase => {
                purchase.current = purchase.end_date ? moment(purchase.end_date).isAfter(now) : true;
            });
            $scope.gridProductPurchasesOptions.data = purchases;
        });
    $scope.purchasesTracker.addPromise(purchasesProducts);

    $scope.gridServicePurchasesOptions = {
        data: [],
        paginationPageSize: 9,
        paginationPageSizes: [9],
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        isCollapsed: false,
        columnDefs: [
            {
                displayName: 'Order Date',
                field: 'order_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '10%'
            }, {
                displayName: 'Start Date',
                field: 'start_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '10%'
            }, {
                displayName: 'Description',
                field: 'description',
                tooltip: true,
                width: '50%'
            }, {
                displayName: 'Type',
                field: 'product_type',
                width: '30%'
            }
        ]
    };

    var purchasesServices = contextService.getPurchaseServiceList(accountId)
        .then(purchases => {
            _.each(purchases, purchase => {
                purchase.current = true;
                purchase.expiredClass = '';
                if (purchase.end_date) {
                    if (moment(purchase.end_date).isBefore(moment())) {
                        purchase.current = false;
                        purchase.expiredClass = 'bg-danger';
                    }
                }
            });
            $scope.purchasesProducts = purchases;
            $scope.gridServicePurchasesOptions.data = purchases;
        });
    $scope.purchasesTracker.addPromise(purchasesServices);


    //API Proxies
    var apiProxiesTracker = promiseTracker();

    // <td><a href="#">{{ proxy.api_name }}</a></td>
    // <td>{{ proxy.created_date | date : 'yyyy-MM-dd' }}</td>
    // <td>{{ proxy.revision }}</td>
    // <td class='bg-danger'>{{proxy.bundlelinter_title}} ({{proxy.bundlelinter_score}})</td>
    // <td nowrap class='bg-danger'>
    //     Prod-Prod<br />
    //     Prod-Stage<br />
    //     Nonprod-QAT<br />
    //     Nonprod-Dev
    // </td>
    // <td class='bg-danger'><jq-sparkline data="proxy.traffic_sparkline" options="jqSparklineOptions"></jq-sparkline></td>

    $scope.gridProxiesOptions = {
        data: [],
        paginationPageSize: 25,
        paginationPageSizes: [25, 50, 75],
        enablePaginationControls: true,
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        rowHeight: 30,
        columnDefs: [
            {
                displayName: 'Org',
                field: 'org_name',
                cellTooltip: true,
                width: '15%'
            }, {
                displayName: 'API',
                field: 'api_name',
                cellTooltip: true,
                width: '15%'
            }, {
                displayName: 'Created',
                field: 'created_date',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '10%'
            }, {
                displayName: 'Rev',
                field: 'revision',
                headerTooltip: true,
                width: '5%'
            }, {
                displayName: 'Score',
                field: 'bundle_score',
                width: '15%',
                cellTooltip: true,
                cellTemplate: '<span ng-if="row.entity.bundle_score > 0" class="ui-grid-cell-contents">{{ row.entity.bundle_label }} {{ row.entity.bundle_score }}</span>'
            }, {
                displayName: '90d Avg Traffic',
                headerTooltip: true,
                field: 'traffic_90d_avg',
                type: 'number',
                width: '5%'
            }, {
                displayName: 'Traffic',
                field: 'traffic_sparkline',
                enableSort: false,
                width: '30%',
                cellTemplate: '<jq-sparkline ng-if="!row.groupHeader" data="row.entity.traffic_sparkline" options="small">',
            }
        ]
    };

    var apiProxies = contextService.getApiProxyList(accountId);
    apiProxies.then(list => {
        $scope.gridProxiesOptions.data = list;
    })
    apiProxiesTracker.addPromise(apiProxies);
    $scope.apiProxiesTracker = apiProxiesTracker;


    //API Products
    var apiProductsTracker = promiseTracker();

    $scope.gridProductsOptions = {
        data: [],
        paginationPageSize: 25,
        paginationPageSizes: [25, 50, 75],
        enablePaginationControls: true,
        enableFiltering: true,
        enableColumnMenus: false,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        rowHeight: 30,
        columnDefs: [
            {
                displayName: 'Org',
                field: 'org_name',
                tooltip: true,
                width: '15%'
            }, {
                displayName: 'Product',
                field: 'displayname',
                tooltip: true,
                width: '30%'
            }, {
                displayName: 'Created',
                field: 'createddate',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '10%'
            }, {
                displayName: 'Traffic',
                field: 'traffic_sparkline',
                width: '55%',
                cellTemplate: '<jq-sparkline ng-if="!row.groupHeader" data="row.entity.traffic_sparkline" options="small">'
            }
        ]
    };

    var apiProducts = contextService.getApiProductList(accountId);
    apiProducts.then(list => {
        $scope.gridProductsOptions.data = list;
    })
    apiProductsTracker.addPromise(apiProducts);
    $scope.apiProductsTracker = apiProductsTracker;

    $timeout(function(){
        var tab = $('#overviewTab');
        tab.removeClass('active');
    }, 250);
    }
};