'use strict';

var moment = require('moment');
var $ = require('jquery');
var d3 = require('d3');

/**
 * @ngInject
 */
module.exports = class OverviewController {
    constructor ($rootScope, $state, $scope, $location, tsAuthService,
    promiseTracker, contextService, $window, $timeout, $stateParams) {
    'ngInject';

    var accountId = $stateParams.accountId;
    $scope.accountId = accountId;

    $scope.accountTracker = promiseTracker();
    $scope.account_entitlements = [];

    $scope.collapseTracker = {}

    var account = contextService.getAccount(accountId)
        .then(account => {
            $scope.account = account;
            $scope.account_entitlements = [];

            if (ENV != 'local') {
                _.each(account.product_plan_ids, account_entitlement_id => {
                    var account_entitlement = contextService.getAccountEntitlement(account_entitlement_id, true)
                        .then(entitlement => {
                            if (entitlement) {
                                $scope.collapseTracker[entitlement.id] = true;
                                $scope.account_entitlements.push(entitlement);
                            }
                        });
                    $scope.accountTracker.addPromise(account_entitlement);
                });
            } else {
                let entitlements = require('./mock_entitlements');
                $scope.account_entitlements = entitlements;
            }
        });
    $scope.accountTracker.addPromise(account);

    $scope.pendingAccountPlanUpdate = false;

    var updateAccountPlan = function () {

        // console.log('Updating Expansion Target');
        var baseline = _.clone($scope.account_plan);

        contextService.updateAccountPlan($scope.originalAccountPlan, $scope.account_plan)
            .then(account_plan => {
                $scope.originalAccountPlan = baseline;
                // console.log('Updated Expansion Plan');
                $rootScope.$broadcast('EXPANSION_TARGET_UPDATED',
                    { id: $scope.accountId, expansion_target: $scope.account_plan.expansion_target }
                );
            })
            .catch(err => {
                console.error('Error updating expansion target');
            });

    }

    $scope.$watch('account_plan', (new_value, old_value) => {
        if ($scope.account_plan_loaded) {
            // if(new_value == old_value) return;
            if ($scope.pendingAccountPlanUpdate !== false) {
                $timeout.cancel($scope.pendingAccountPlanUpdate);
                $scope.pendingAccountPlanUpdate = false;
            }
            $scope.pendingAccountPlanUpdate = $timeout(updateAccountPlan, 1500);
        }
    }, true);

    $scope.accountPlanTracker = promiseTracker()
    var account_plan = contextService.getAccountPlan(accountId)
        .then(account_plan => {
            $scope.account_plan = account_plan;
            $scope.originalAccountPlan = _.clone(account_plan);
            $timeout(function () { $scope.account_plan_loaded = true; }, 100);
        })

    $scope.accountPlanTracker.addPromise(account_plan);
    }
};