'use strict';

var angular = require('angular'),
  angularMoment = require('angular-moment'),
  angularPromiseTracker = require('angular-promise-tracker'),
  angularUiRouter = require('@uirouter/angularjs').default,
  uiGrid = require('angular-ui-grid').default,
  angularAsyncAwait =require("angular-async-await");
  ;
// var format = require('format-duration');
var moment = require('moment');
var _ = require('lodash');



module.exports = angular.module('app.customers.detail.overview', [
    angularMoment,
    angularPromiseTracker,
    angularUiRouter,
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.pagination',
    'angular-async-await',
    require('tsbusy').name,
    require('tsuigrid').name,
    require('jqsparkline').name,
    require('radarchart').name
])
  .config(function($stateProvider) {
    $stateProvider
      .state('customers.detail.overview', {
        url: '/overview',
        component: 'overview'
      });
  })
  .filter('bool', function() {
    return function(input) {
        return input ? 'Yes' : 'No';
    }
  })
  .filter('list_in_text', ['$filter', function($filter) {
    return function(input) {
      if(input == null || input == undefined) return input;
      if((''+input).indexOf('•') > -1){
        var items = input.split('•');
        _.each(items, item =>{
          item = item.trim();        
        });
        input = items.join('<br/>');
      }
      return input;
    };
  }])
  .filter('sla_target', ['$filter', function ($filter) {
    return function (input) {
      var parsed = parseFloat(input);
      if(!isNaN(parsed)){
        return moment.duration(parsed * 60*1000).humanize();
      }
      if(input == null){
        return;
      }

      if(''+input != ''){
        return input + ' minutes';
      }

      return input;
    };
  }])  
  .component('overview', require('./overview.component'));

