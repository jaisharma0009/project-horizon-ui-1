'use strict';

var angular_mock = require('angular-mocks');

module.exports = function() {
  var module;

  // initialize the app module
  beforeEach(angular_mock.inject(function($controller, $rootScope) {
    module = angular.module('app');
  }));

  it('should be registered', function() {
    expect(module).to.exist;
  });

  it('should have provide a version string', function() {
    expect(module.constant('value')).to.be.a.string;
  });

};
