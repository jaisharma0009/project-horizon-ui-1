var angular_mock = require('angular-mocks'),
  kanban_mock = require('../mocks/kanban.mock'),
  $ = require('jquery');

module.exports = function () {
  beforeEach(angular.mock.module('app'));
  beforeEach(angular.mock.module('app.kanban'));

  var ctrl, element, scope;

  beforeEach(angular_mock.inject(function ($controller, $rootScope, $componentController, $compile) {
    scope = $rootScope.$new();
    element = angular.element('<kanban></kanban>');
    element = $compile(element)(scope);

    ctrl = $componentController('kanban', {$scope: scope}); 
    ctrl.kanbanService = kanban_mock;   
    scope.$digest();
  }));

  it('should load the kanban template', function () {
    expect(element.find('h1').text()).to.equals('CS Quarterly Kanban View');
  });

  it('controller should exist', () => {
    expect(ctrl).to.exist;
  });

  it('should load and watch for filters', done => {
    this.timeout(10000);

    expect(ctrl.scope.opportunities.length).to.be.an('undefined');
    

    setTimeout(() => {
      try {
        expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(38);
        
        scope.filters.selectedProbability = 80;
        scope.$apply();
        
        setTimeout(() => {
          expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(3);
          done();
        }, 100);
        
      } catch (e) { done(e); }
    }, 100);
    
  });

  it('should find the opportunity', done => {
    this.timeout(10000);
    ctrl.getOpportunities(ctrl);

    setTimeout(() => {
      var opp = ctrl.findOpportunityById('k9UJG1xWKADWv59');
      expect(opp.selType).to.equal('Sales');
      expect(opp.selQuarter).to.equal('17 Q3');
      expect(opp.selIndex).to.equal(34);

      var opp = ctrl.findOpportunityById('doesnt exist');
      expect(opp).to.be.an('undefined');
     
      done();
    }, 1000);
  });

  it('initQuarter should initiate the array', () => {
    let opps = [];
    let quarter = 'test';
    expect(opps[quarter]).to.be.an('undefined');

    ctrl.initQuarter(opps, quarter);
    expect(opps[quarter]).to.be.an('array');
  });

  it('dndMoved should work', done => {
    this.timeout(10000);
    ctrl.getOpportunities(ctrl);

    setTimeout(() => {
      let length1 = ctrl.scope.opportunities.Sales['17 Q4'].length;
      let lenght2 = ctrl.scope.opportunities.Sales['17 Q3'].length;
      let sel = ctrl.scope.opportunities.Sales['17 Q3'][34];

      ctrl.scope.opportunities.Sales['17 Q4'].push(sel);
      expect(ctrl.dragHistory).to.be.an('array');
      expect(ctrl.dragHistory.length).to.equal(0);

      ctrl.dndMoved(34, '17 Q3', 'Sales');
      expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(length1 + 1);
      expect(ctrl.scope.opportunities.Sales['17 Q3'].length).to.equal(lenght2 - 1);
      expect(ctrl.dragHistory).to.be.an('array');
      expect(ctrl.dragHistory.length).to.equal(1);

      ctrl.restoreLastDrag();
      expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(length1);
      expect(ctrl.scope.opportunities.Sales['17 Q3'].length).to.equal(lenght2);
      expect(ctrl.dragHistory).to.be.an('array');
      expect(ctrl.dragHistory.length).to.equal(0);

      done();
    }, 1000);
  });

  it('restoreDragAll should work', done => {
    this.timeout(10000);
    ctrl.getOpportunities(ctrl);

    setTimeout(() => {
      let length1 = ctrl.scope.opportunities.Sales['17 Q4'].length;
      let sel1 = ctrl.scope.opportunities.Sales['17 Q3'][34];
      let sel2 = ctrl.scope.opportunities.Sales['17 Q3'][35];

      ctrl.scope.opportunities.Sales['17 Q4'].push(sel1);
      ctrl.dndMoved(34, '17 Q3', 'Sales');

      ctrl.scope.opportunities.Sales['17 Q4'].push(sel2);
      ctrl.dndMoved(34, '17 Q3', 'Sales');

      expect(ctrl.dragHistory).to.be.an('array');
      expect(ctrl.dragHistory.length).to.equal(2);
      expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(length1 + 2);

      ctrl.restoreDragAll();
      expect(ctrl.dragHistory).to.be.an('array');
      expect(ctrl.dragHistory.length).to.equal(0);
      expect(ctrl.scope.opportunities.Sales['17 Q4'].length).to.equal(length1);

      done();
    }, 1000);
  });

};
