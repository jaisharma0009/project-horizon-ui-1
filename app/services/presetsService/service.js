'use strict';

module.exports = function ($stateParams, $q) {
  'ngInject';

  var self = this;

  this.getAll = function() {
    var deferred = $q.defer();

    var filters = JSON.parse(localStorage.getItem('states')) || [];
		filters.unshift({ title: 'All', name: 'all' });
		filters.push({ title: 'Export selected', name: 'export' });
		//filters.push({ title: 'Import presets', name: 'import' });

    deferred.resolve(filters);

    return deferred.promise;
  };

  this.getById = function(id) {
    var deferred = $q.defer();

    var filters = JSON.parse(localStorage.getItem('states')) || [];
    var result = _.find(filters, item => item.id == id);
    deferred.resolve(result);

    return deferred.promise;
  }

  this.save = function(preset) {
    var deferred = $q.defer();

    var filters = JSON.parse(localStorage.getItem('states')) || [];

    preset.id = guid();
    filters.push(preset);

    deferred.resolve(filters);

    return deferred.promise;
  };

  function guid() {
    let s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  }

}
