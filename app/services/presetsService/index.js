'use strict';

var angular = require('angular');

module.exports = angular.module('app.customers.services.presetsService', [])
  .service('presetsService', require('./service'));