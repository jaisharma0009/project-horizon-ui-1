'use strict';

var angular = require('angular');

module.exports = angular.module('app.customers.services.kanbanService', [])
  .service('kanbanService', require('./service'));