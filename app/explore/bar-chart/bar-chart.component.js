let BarChartController = require('./bar-chart.controller');

let BarChartComponent = {
  restrict: 'E',
  controller: BarChartController,
  controllerAs: '$ctrl',
  bindings: {
    axis: '@',
    class: '@',
    chartSize: '@',
    chartId: '@',
    type: '@',
    chartOrientation: '@',
    data: '='
  }
};

module.exports = BarChartComponent;