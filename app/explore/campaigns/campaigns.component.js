var template = require('./campaigns.template.html');
var CampaignsController = require('./campaigns.controller');

let CampaignsComponent = {
  restrict: 'E',
  template,
  controller: CampaignsController,
  controllerAs: '$ctrl',
  bindings: {
    type: '@',
    data: '='
  }
};

module.exports = CampaignsComponent;