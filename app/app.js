'use strict';

import Directives from './directives';

var angular = require('angular'),
  angularUiRouter = require('@uirouter/angularjs').default,
  angulartics = require('angulartics'),
  angularticsgtm = require('angulartics-gtm'),
  restangular = require('restangular'),
  _ = require('lodash'),
  angularStorage = require('angular-storage/dist/angular-storage'),
  angularMaterial = require('angular-material'),
  angularBootstrap = require('angular-bootstrap'),
  $ = require('jquery'),
  values = require('./values');


require('angular-material/angular-material.css');
require('./less/app.less');
require('angular-ui-grid/ui-grid.css');

var mode = ENV || 'prod';

var base_api_url = 'http://localhost:9090';
base_api_url = 'https://nucleus-api-test.apigee.com/nucleus-reporting-api';

var cs_api_url = 'https://nucleus-api-test.apigee.com/cs-api';
// cs_api_url = 'http://localhost:9090';

if (mode === 'local') {
  base_api_url = 'http://localhost:3333/nucleus-reporting-api';
  cs_api_url = 'http://localhost:3333/nucleus-reporting-api';
  base_api_url = 'http://localhost:8080';
  cs_api_url = 'http://localhost:8080';
}

module.exports = angular.module('app', [
  angularUiRouter,
  angulartics,
  angularticsgtm,
  restangular,
  angularMaterial,
  angularBootstrap,
  require('tssidebar').name,
  require('uigridcards').name,
  require('tsautogrow').name,
  require('tscollapsable').name,
  require('tsclickoutside').name,
  require('tsmaterializegrid').name,
  require('tshtmlmodal').name,
  require('tsalerts').name,
  require('tsselect').name,
  require('tsauth').name,
  require('tssearch').name,
  require('tsnavbar').name,
  require('./customers').name,
  require('./kanban').name,
  require('./login').name,
  require('./engage').name,
  require('./explore').name,
  require('uigridcolsdropdown').name,
  Directives
])
  .filter('htmldecode', require('./filters').htmldecode)
  .constant('APP_VERSION', require('json-loader!../package.json').version)
  .constant('BASE_API_URL', base_api_url)
  .constant('CS_API_URL', cs_api_url)
  .constant('JQSPARKLINE_DEFAULT_OPTIONS', {
    type: 'line',
    width: '100%',
    height: '1.5em',
    chartRangeMin: 0,
    defaultPixelsPerValue: 2,

    lineColor: '#298fc2',
    fillColor: 'rgba(41, 143, 194, 0.1)',
    spotColor: '',
    minSpotColor: null,
    maxSpotColor: '#fc4c02',
    highlightSpotColor: '#fc4c02',
    highlightLineColor: '#fc4c02',

    disableHiddenCheck: true,
    enableTagOptions: true,

    zeroAsNull: true
  })
  .factory('jqSparklineOptions', function (JQSPARKLINE_DEFAULT_OPTIONS, $rootScope) {

    var options = {
      default: JQSPARKLINE_DEFAULT_OPTIONS,
      small: _.merge(_.clone(JQSPARKLINE_DEFAULT_OPTIONS)),
      medium: _.merge(_.clone(JQSPARKLINE_DEFAULT_OPTIONS), {
        height: '50px',
        width: '100%'
      }),
      large: _.merge(_.clone(JQSPARKLINE_DEFAULT_OPTIONS), {
        height: '75px',
        width: '100%'
      })
    };
    $rootScope.jqSparklineOptions = options;
    return options;
  })
  .config(function ($mdThemingProvider) {
    var neonRedMap = $mdThemingProvider.extendPalette('red', {
      '500': '#000',
      'contrastDefaultColor': 'dark'
    });

    $mdThemingProvider.definePalette('black', neonRedMap);
    $mdThemingProvider.theme('default').primaryPalette('black');
  })
  .config(function (APP_VERSION) {
    console.log('Google Apigee CS Context v' + APP_VERSION);
  })
  .config(function (RestangularProvider, BASE_API_URL) {
    RestangularProvider.setBaseUrl(BASE_API_URL);

    RestangularProvider.addResponseInterceptor(function (response, operation,
      what, url) {

      if (typeof response === 'object') {
        // unwrap response data
        if (!Array.isArray(response) &&
          response.hasOwnProperty('data')) {
          response = response.data;
        }

        // unwrap resources returned as an array with a single item
        if (operation === 'get' &&
          Array.isArray(response) &&
          response.length === 1) {
          response = response[0];
        }
      }
      return response;

    });

    RestangularProvider.setDefaultHttpFields({
      withCredentials: true // send and receive cookies/HTTP auth
      // cache: true
    });
  })
  .factory('csRestangular', ['Restangular', '$window', function (Restangular, $window) {
    Restangular.setDefaultHeaders({ Authorization: $window.localStorage.getItem('Authorization') });
    var csRest = Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl(cs_api_url);
      // RestangularConfigurer.setBaseUrl('http://localhost:9090');
    });
    csRest.addResponseInterceptor(function (responseData,
      operation, what, url, response, deferred) {
      if (typeof responseData === 'object') {
        // unwrap response data
        if (!Array.isArray(responseData) && responseData.hasOwnProperty(
          'entities')) {
          responseData = responseData.entities;
        }

        // unwrap resources returned as an array with a single item
        if (operation === 'get' &&
          Array.isArray(responseData) &&
          responseData.length === 1) {
          responseData = responseData[0];
        }
      }

      return responseData;
    });
    return csRest;
  }])
  .config(function ($stateProvider, $urlRouterProvider) {
    // $urlRouterProvider.otherwise('/login');
    $urlRouterProvider.otherwise('/engage');
  })
  .run(function ($rootScope, $state, $stateParams, Restangular, csRestangular, APP_VERSION,
    tsAlertsService, tsAuthService, $timeout, $location, $templateCache, $document) {
      let scrollEvent = false;

    window.addEventListener('resize', () => {
      $rootScope.$broadcast('RESIZE_WINDOW');
    });

    $(window).scroll(function (event) {
			let scrollHeight = document.body.scrollHeight;
    	let totalHeight = window.scrollY + window.innerHeight;

			if(totalHeight >= scrollHeight) {
        if(!scrollEvent) {
          scrollEvent = true;
          $rootScope.$broadcast('SCROLL_BOTTOM');

          setTimeout(() => {
            scrollEvent = false;
          }, 500);
        }
    	}
		});

    $rootScope.$watch(() => $state.current.name, (newState, oldState) => {
      $rootScope.$broadcast('STATE_CHANGED', { newState, oldState });

      let mf = $('#main-footer');
      (newState.indexOf('customers.detail') != -1) ? mf.hide() : mf.show();

      if (newState != 'login') {
        localStorage.setItem('lastState', newState);
        localStorage.setItem('lastStateUrl', $location.url());
      } else {
        let current = 'engage';
        window.localStorage.setItem('lastState', current);
        window.localStorage.setItem('lastStateUrl', '/engage');
      }
    });

    let keyPress = e => {
      let evtobj = window.event ? event : e;

      if (evtobj.ctrlKey) {
        switch (evtobj.keyCode) {
          case 90:
            $rootScope.$broadcast('KEYS_UNDO', evtobj);
            break;
        }
      } else {
        $rootScope.$broadcast('KEY_PRESS', evtobj);
      }
    }
    document.onkeydown = keyPress;

    // Auth and error alerts
    Restangular.setErrorInterceptor(function (response, promise) {
      if (ENV === 'local') {
        return false;
      }

      if (response.status === 401) {

        if ($state.current.name != 'login') {
          let current = $state.current.name || 'customers';
          window.localStorage.setItem('lastState', current);
          window.localStorage.setItem('lastStateUrl', $location.url());
        } else {
          let current = 'customers';
          window.localStorage.setItem('lastState', current);
          window.localStorage.setItem('lastStateUrl', '/customers');
        }

        // Re-authenticate when unauthorized
        tsAuthService.clearCredentials();
        $state.go('login');

        return false;
      } else {
        // show all other api errors
        tsAlertsService.add({
          type: 'danger',
          msg: 'Backend error: ' +
          response.config.method + ' ' +
          response.config.url + ' ' +
          response.status + ': ' +
          response.data
        });
      }
      return true;
    });

    // Auth and error alerts
    csRestangular.setErrorInterceptor(function (response, promise) {
      if (ENV === 'local') {
        return false;
      }

      if (response.status === 401) {

        // Re-authenticate when unauthorized
        tsAuthService.clearCredentials();
        $state.go('login');
        return false;
      } else {
        // show all other api errors
        tsAlertsService.add({
          type: 'danger',
          msg: 'Backend error: ' +
          response.config.method + ' ' +
          response.config.url + ' ' +
          response.status + ': ' +
          response.data
        });
      }
      return true;
    });

    // This get's called when the app loads and if it has just been authorized, it will grab the
    //  authorization information from the headers for building the Bearer token and the uuid in the headers
    if (mode != 'dev' && mode != 'local') {
      tsAuthService.redirect();
    }

    // For auth to work, all requests to the API need to have a Bearer token as well as a UUID header.
    //  This will add it...
    Restangular.addFullRequestInterceptor(tsAuthService.fullRequestInterceptor);

    $rootScope.tsAuth = tsAuthService;
    $rootScope.currentYear = new Date().getFullYear();
    // Misc/debug
    $rootScope.navBarCollapsed = true;

    $rootScope.APP_VERSION = APP_VERSION;

    // $rootScope.BASE_API_URL = BASE_API_URL;

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $templateCache.put('ui-grid/pagination', values.paginationTemplate);

  });
