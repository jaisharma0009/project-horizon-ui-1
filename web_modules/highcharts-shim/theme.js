'use strict';

module.exports = function(Highcharts) {
  Highcharts.theme = {
    colors: [
      '#fc4c02', // Apigee Orange
      '#298fc2', // Apigee Blue
      '#009f4d', // Apigee Green

      // Additional Apigee colors
      '#813b96',
      '#faa438',
      '#59595b',
      '#7a9543', // Analytics
      '#5287a2', // Dev Adoption
      '#d3990a', // Security OAuth
      '#7c5377', // Digital Strategy
      '#8c6239', // API Design
      '#ac5d5f', // Mobile Apps
      '#4a5981'  // Product

      /*
      // greys
      '#4a494a', // extra
      '#54585a',
      '#707372',
      '#a2aaad',
      '#d0d3d4',
      '#e5e1e6',
      '#EDEBEE', // extra
      '#F6F5F6', // extra
      */
    ],

    chart: {
      animation: false,
      style: {
        fontFamily: 'inherit'
      },
      spacing: [15, 0, 15, 0] // top right bottom left
    },
    title: {
      style: {
        color: '#707372', // @apigee-grey-02
        fontSize: '16pt'
      }
    },
    subtitle: {
      style: {
        color: '#707372', // @apigee-grey-02
        fontSize: '12pt'
      }
    },
    xAxis: {
      // gridLineColor: '#e5e1e6', // @apigee-grey-05
      // gridLineWidth: 1,
      labels: {
        style: {
          color: '#707372', // @apigee-grey-02
          fontSize: '9pt'
        }
      },
      // lineColor: '#e5e1e6', // @apigee-grey-05
      lineWidth: 0,
      tickColor: '#e5e1e6', // @apigee-grey-05
      title: {
        style: {
          color: '#707372', // @apigee-grey-02
          fontSize: '11pt'
        }
      }
    },
    yAxis: {
      gridLineColor: '#e5e1e6', // @apigee-grey-05
      gridLineWidth: 1,
      labels: {
        style: {
          color: '#707372', // @apigee-grey-02
          fontSize: '9pt'
        }
      },
      lineColor: '#e5e1e6', // @apigee-grey-05
      // tickColor: '#e5e1e6', // @apigee-grey-05
      tickWidth: 0,
      title: {
        style: {
          color: '#707372', // @apigee-grey-02
          fontSize: '11pt'
        }
      }
    },
    tooltip: {
      backgroundColor: 'rgba(0,0,0,0.75)',
      pointFormatter: function(){
        return '<span style="color:'+this.color+'">\u25CF</span> '+this.series.name+': <b>'+this.y.toLocaleString()+'</b><br/>';
      },
      style: {
        color: '#fff'
      }
    },
    legend: {
      // backgroundColor: '#f6f5f6', // @apigee-grey-07
      margin: 10,
      itemStyle: {
        color: '#707372', // @apigee-grey-02
        fontWeight: 'normal',
        fontSize: '10pt'
      },
      itemHoverStyle: {
        color: '#54585a' // @apigee-grey-01
      },
      itemHiddenStyle: {
        color: '#d0d3d4' // @apigee-grey-04
      }
    },
    credits: {
      enabled: false
      /*
      style: {
        color: '#e5e1e6' // @apigee-grey-05
      }
      */
    },
    labels: {
      style: {
        color: '#707372' // @apigee-grey-02
      }
    },
    plotOptions: {
      area: {
        fillOpacity: 0.1
      },
      bar: {
        borderWidth: 0
      },
      column: {
        borderWidth: 0
      }
    },
    // special colors for some of the
   //legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
  };

  // Apply the theme
  Highcharts.setOptions(Highcharts.theme);
};
