// Karma configuration
var webpackConfig = require('./webpack/config/webpack.dev').default;

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai'],
    files: [ 'app/**/*_test.js' ],
    exclude: [ '**/*.swp' ],
    preprocessors: {
      'app/**/*_test.js': ['webpack']
    },
    webpack: webpackConfig({env: 'local'}),
    webpackServer: require('./webpack.server.config'),
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Firefox'],
    singleRun: false
  });
};
