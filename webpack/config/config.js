import path from 'path';

export default {
  // relative to the root dir
  paths: {
    app: path.resolve(__dirname, '../..', 'app'),
    dist: path.resolve(__dirname, '../..', 'dist'),
    siteDist: '../site/dist',
    site: '../site',
    siteZip: '../site.zip',
    static: 'static',
    bower_components: 'bower_components'
  }
};
