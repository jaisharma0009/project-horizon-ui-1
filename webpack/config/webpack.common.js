import path                 from 'path';
import webpack              from 'webpack';
import HtmlWebpackPlugin    from 'html-webpack-plugin';
import ExtractTextPlugin    from 'extract-text-webpack-plugin';
import ProgressBarPlugin    from 'progress-bar-webpack-plugin';
import FriendlyErrorsPlugin from 'friendly-errors-webpack-plugin';
import config               from './config';

let entry = path.join(config.paths.app, 'app.js');

export default function (options) {

  const extractLess = new ExtractTextPlugin({
    filename: `[name].bundle.[hash].css`,
    disable: 'local' === options.env || 'dev' === options.env,
    allChunks: true
  });

  return {
    entry,

    devtool: '#cheap-module-eval-source-map',

    output: {
      path: config.paths.dist,
      filename: '[name].bundle.[hash].js',
      chunkFilename: '[id].bundle.[chunkhash].js',
      // namedChunkFilename: '[name].bundle.[hash].js',
    },

    cache: true,

    resolve: {
      extensions: ['.js', '.json'],
      alias: {
        '@': config.paths.app,
        'angular-bootstrap': 'angular-bootstrap/ui-bootstrap-tpls',
        'angulartics': 'angulartics',
        'angulartics-ga': 'angulartics/src/angulartics-ga',
        'angulartics-gtm': 'angulartics/src/angulartics-gtm',
        'footable': 'footable',
        'footable/footable.sort': 'footable/js/footable.sort',
        'jquery.sparkline': 'jquery-sparkline',
        'uri.js': 'uri.js/src/uri.js'
      },
      modules: [
        'node_modules',
        'web_modules',
        path.join(config.paths.app, 'components')
      ],
    },
    module: {
      rules: [
        {
          test: /\/angular(?:\.min)?\.js$/,
          use: [ 'exports-loader?angular' ]
        }, {
          test: /\/angular-file-saver(?:\.min)?\.js$/,
          use: [ 'exports-loader?FileSaver' ]
        },
        // {
        //   test: require.resolve('jquery'),
        //   use: [{
        //     loader: 'expose-loader',
        //     options: 'jQuery'
        //   }, {
        //     loader: 'expose-loader',
        //     options: '$'
        //   }]
        // },
        // {
        //   test: require.resolve('angular'),
        //   use: [{
        //     loader: 'expose-loader',
        //     options: 'angular'
        //   }]
        // },
        {
          test: /\.js$/,
          use: [
            {
              loader: 'babel-loader'
            }
          ],
          include: [
            config.paths.app
          ]
        }, {
          test: /\/angular(?:\.min)?\.js$/,
          loader: 'exports-loader?angular'
        }, {
          test: /\/angular-file-saver(?:\.min)?\.js$/,
          loader: 'exports-loader?FileSaver'
        }, {
          test: /\/angular-sanitize(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ngSanitize"'
          ]
        }, {
          test: /\/angular-animate(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ngAnimate"'
          ]
        }, {
          test: /\/angular-cookie(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ipCookie"'
          ]
        }, {
          test: /\/angular-cookies(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ngCookies"'
          ]
        }, {
          test: /\/angular-mocks(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?angular.mock'
          ]
        }, {
          test: /\/angular-moment(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?moment=moment',
            'exports-loader?"angularMoment"'
          ]
        }, {
          test: /\/angularjs-gravatardirective(?:.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"angularjs-gravatardirective"'
          ]
        }, {
          test: /\/angular-wizard(?:\.min)?\.js$/,
          loader: 'imports-loader?angular=angular'
        }, {
          test: /\/angularLocalStorage\.js$/,
          loaders: [
            'imports-loader?angular=angular,ngCookies=angular-cookies',
            'exports-loader?"angularLocalStorage"'
          ]
        }, {
          test: /\/footable(?:\.all)?\.js$/,
          loaders: [
            'imports-loader?jQuery=jquery',
            'exports-loader?window.footable'
          ]
        }, {
          test: /\/highstock(?:\.src)?\.js$/,
          loaders: [
            // 'imports-loader?jQuery=jquery' // satisfied by jQuery expose loader
            'exports-loader?Highcharts,HighchartsAdapter'
          ]
        }, {
          test: /\/highstock\/modules\/(?:[^\/]*)?\.js$/,
          loaders: [
            'imports-loader?Highcharts=>require("highcharts-shim").Highcharts'
          ]
        }, {
          test: /\/highcharts-ng(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular,Highcharts=>require("highcharts-shim").Highcharts',
            'exports-loader?"highcharts-ng"'
          ]
        }, {
          test: /\/promise-tracker\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ajoslin.promise-tracker"'
          ]
        }, {
          test: /\/angulartics\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"angulartics"'
          ]
        }, {
          test: /\/angulartics-ga\.js$/,
          loaders: [
            'imports-loader?angular=angular&angulartics=angulartics',
            'exports-loader?"angulartics.google.analytics"'
          ]
        }, {
          test: /\/angulartics-gtm\.js$/,
          loaders: [
            'imports-loader?angular=angular&angulartics=angulartics',
            'exports-loader?"angulartics.google.tagmanager"'
          ]
        }, {
          test: /\/restangular(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular&_=lodash',
            'exports-loader?"restangular"'
          ]
        }, {
          test: /\/ui-bootstrap(?:-tpls)?(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?angular=angular',
            'exports-loader?"ui.bootstrap"'
          ]
        },
        {
          test: /\/jquery(?:\.min)?\.js$/,
          loaders: [
            'expose-loader?jQuery' // for libraries that expect window.jQuery (e.g. highcharts)
          ]
        },
        {
          test: /\/jquery.sparkline(?:\.min)?\.js$/,
          loaders: [
            'imports-loader?$=jquery',
            'exports-loader?"jquery.sparkline"'
          ]
        }, {
          test: /\.html$/,
          use: [{
            loader: 'html-loader',
            options: {
              /**
               * Set to true or comment attrs to allow webpack handle images on html
               * @type {Boolean}
               */
              attrs: ['img:src'],
              // attrs: [ 'img:src', 'img:ng-src', 'img:data-src' ],
              interpolate: 'require',
              minimize: true,
              root: '.',
            }
          }],
        }, {
          test: /\.css$/,
          use: extractLess.extract({
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader',
              options: {
                // minimize: false,
                url: true,
                import: false,
                modules: false,
              }
            }]
          })
        }, {
          test: /\.less/,
          use: extractLess.extract({
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader',
              options: {
                // minimize: false
              }
            }, {
              loader: 'postcss-loader',
              options: { }
            }, {
              loader: 'less-loader',
              options: { }
            }]
          })
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?[a-z0-9=\.]+)?$/,
          use: [{
            loader: 'url-loader',
            options: {
              limit: 40000,
              name: 'assets/imgs/[name].[ext]?[hash]',
            }
          }]
          // loader: 'url-loader?limit=40000&name=assets/images/[name].[ext]?[hash]'
        },
        {
          // Loader for legacy fonts (old browsers)
          test: /\.(woff|eot|ttf|otf)(\?[a-z0-9=\.]+)?$/,
          use: [{
            loader: 'file-loader',
            options: {
              limit: 20000,
              name: 'assets/fonts/[name].[ext]?[hash]',
            }
          }]
        }, {
          // Loader for fonts in modern browsers
          test: /\.(woff2)(\?[a-z0-9=\.]+)?$/,
          use: [{
            loader: 'url-loader',
            options: {
              limit: 40000,
              name: 'assets/fonts/[name].[ext]?[hash]',
            }
          }]
        },
      ],
      noParse: [
        // don't scan libraries for require/define calls (they don't have any,
        // unless they use an imports loader)
        /\/highcharts(?:\.src)?\.js$/,
        /\/highstock(?:\.src)?\.js$/,
        /\/moment(?:-with-langs)?(?:\.min)?\.js$/
      ],
    },
    plugins: [
      // Exclude all moment locale files, except english and spanish languages
      new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|es)$/),

      extractLess,

      // Generate index.html
      new HtmlWebpackPlugin({
        filename: 'index.html',
        inject: true,
        template: path.join(config.paths.app, 'index.template.html')
      }),

      new ProgressBarPlugin(),

      new webpack.DefinePlugin({
        'ENV': JSON.stringify(options.env)
      }),

      new webpack.ProvidePlugin({
        $: 'jquery'
        , jQuery: 'jquery'
      }),

      new FriendlyErrorsPlugin(),
    ]
  };
}
